<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\cache\driver\Redis;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * Win模型对象
     * @var \app\admin\model\Win
     */
    protected $model = null;

    public function _initialize()
    {
        $this->model = new \app\admin\model\Win;
    }

    /**
     * 首页
     *
     */
    public function index()
    {
        $redis = new Redis;
        if($redis->get('indexData')) {
            $this->success('ok',$redis->get('indexData'),200);
        }
        $res = $this->model->select();
        $redis->set('indexData',$res);
        $this->success('ok',$res,200);
    }

    /**
     * 获取访问IP
     */
    public function getip()
    {
        $redis = new Redis;
        $ip = $this->request->ip();
        if($redis->get($ip)) {
            $this->success('ok','您已抽过奖了！',401);
        }
        $redis->set($ip,$ip,20);
        $this->success('ok',$redis->get($ip),200);
    }
}
