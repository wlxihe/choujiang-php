<?php

return [
    'Id'         => 'ID',
    'Name'       => '活动名称',
    'Images'     => '奖品图片列表',
    'Json'       => '配置',
    'Json key'   => '名称',
    'Json value' => '值',
    'Image'      => '分享二维码或者推广二维码',
    'Views'      => '点击量',
    'Weigh'      => '排序',
    'Createtime' => '创建时间',
    'Deletetime' => '删除时间',
    'Updatetime' => '更新时间'
];
