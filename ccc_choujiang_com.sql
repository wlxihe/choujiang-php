/*
 Navicat MySQL Data Transfer

 Source Server         : choujiang
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : ccc_choujiang_com

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 27/06/2024 15:18:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fa_admin
-- ----------------------------
DROP TABLE IF EXISTS `fa_admin`;
CREATE TABLE `fa_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
  `logintime` bigint(16) NULL DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录IP',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_admin
-- ----------------------------
INSERT INTO `fa_admin` VALUES (1, 'admin', 'Admin', '0977ea2d60268565809204441721c71e', '7be9b0', 'http://ccc.choujiang.com/assets/img/avatar.png', 'admin@admin.com', '', 0, 1719457931, '127.0.0.1', 1491635035, 1719457931, '542b9716-de97-435d-b467-8ab1b08ff5b0', 'normal');

-- ----------------------------
-- Table structure for fa_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `fa_admin_log`;
CREATE TABLE `fa_admin_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '日志标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_admin_log
-- ----------------------------
INSERT INTO `fa_admin_log` VALUES (1, 1, 'admin', '/RJZXIwPGtm.php/index/login', '登录', '{\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"tppn\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320099);
INSERT INTO `fa_admin_log` VALUES (2, 1, 'admin', '/RJZXIwPGtm.php/addon/install', '插件管理', '{\"name\":\"command\",\"force\":\"0\",\"uid\":\"22607\",\"token\":\"***\",\"version\":\"1.1.2\",\"faversion\":\"1.5.0.20240328\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320222);
INSERT INTO `fa_admin_log` VALUES (3, 1, 'admin', '/RJZXIwPGtm.php/addon/state', '插件管理 / 禁用启用', '{\"name\":\"command\",\"action\":\"enable\",\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320223);
INSERT INTO `fa_admin_log` VALUES (4, 1, 'admin', '/RJZXIwPGtm.php/command/get_field_list', '在线命令管理', '{\"table\":\"fa_admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320649);
INSERT INTO `fa_admin_log` VALUES (5, 1, 'admin', '/RJZXIwPGtm.php/command/get_field_list', '在线命令管理', '{\"table\":\"fa_admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320664);
INSERT INTO `fa_admin_log` VALUES (6, 1, 'admin', '/RJZXIwPGtm.php/command/get_field_list', '在线命令管理', '{\"table\":\"fa_admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320891);
INSERT INTO `fa_admin_log` VALUES (7, 1, 'admin', '/RJZXIwPGtm.php/command/get_field_list', '在线命令管理', '{\"table\":\"fa_test\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320894);
INSERT INTO `fa_admin_log` VALUES (8, 1, 'admin', '/RJZXIwPGtm.php/command/command/action/execute', '在线命令管理 / 生成并执行命令', '{\"commandtype\":\"crud\",\"isrelation\":\"0\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"fa_test\",\"controller\":\"\",\"model\":\"\",\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"tagsuffix\":\"\",\"jsonsuffix\":\"\",\"fixedcolumns\":\"\",\"action\":\"execute\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320905);
INSERT INTO `fa_admin_log` VALUES (9, 1, 'admin', '/RJZXIwPGtm.php/command/command/action/execute', '在线命令管理 / 生成并执行命令', '{\"commandtype\":\"crud\",\"isrelation\":\"0\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"fa_test\",\"controller\":\"\",\"model\":\"\",\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"tagsuffix\":\"\",\"jsonsuffix\":\"\",\"fixedcolumns\":\"\",\"action\":\"execute\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320959);
INSERT INTO `fa_admin_log` VALUES (10, 1, 'admin', '/RJZXIwPGtm.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320963);
INSERT INTO `fa_admin_log` VALUES (11, 1, 'admin', '/RJZXIwPGtm.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320965);
INSERT INTO `fa_admin_log` VALUES (12, 1, 'admin', '/RJZXIwPGtm.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320967);
INSERT INTO `fa_admin_log` VALUES (13, 1, 'admin', '/RJZXIwPGtm.php/command/command/action/execute', '在线命令管理 / 生成并执行命令', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Test.php\",\"action\":\"execute\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719320969);
INSERT INTO `fa_admin_log` VALUES (14, 1, 'admin', '/lollol.php/index/login', '登录', '{\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"3g4l\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388496);
INSERT INTO `fa_admin_log` VALUES (15, 1, 'admin', '/lollol.php/command/get_field_list', '在线命令管理', '{\"table\":\"fa_admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388502);
INSERT INTO `fa_admin_log` VALUES (16, 1, 'admin', '/lollol.php/command/get_field_list', '在线命令管理', '{\"table\":\"fa_win\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388507);
INSERT INTO `fa_admin_log` VALUES (17, 1, 'admin', '/lollol.php/command/command/action/execute', '在线命令管理 / 生成并执行命令', '{\"commandtype\":\"crud\",\"isrelation\":\"0\",\"local\":\"1\",\"delete\":\"0\",\"force\":\"0\",\"table\":\"fa_win\",\"controller\":\"\",\"model\":\"\",\"setcheckboxsuffix\":\"\",\"enumradiosuffix\":\"\",\"imagefield\":\"\",\"filefield\":\"\",\"intdatesuffix\":\"\",\"switchsuffix\":\"\",\"citysuffix\":\"\",\"selectpagesuffix\":\"\",\"selectpagessuffix\":\"\",\"ignorefields\":\"\",\"sortfield\":\"\",\"editorsuffix\":\"\",\"headingfilterfield\":\"\",\"tagsuffix\":\"\",\"jsonsuffix\":\"\",\"fixedcolumns\":\"\",\"action\":\"execute\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388511);
INSERT INTO `fa_admin_log` VALUES (18, 1, 'admin', '/lollol.php/command/command/action/execute', '在线命令管理 / 生成并执行命令', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"\",\"action\":\"execute\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388514);
INSERT INTO `fa_admin_log` VALUES (19, 1, 'admin', '/lollol.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388515);
INSERT INTO `fa_admin_log` VALUES (20, 1, 'admin', '/lollol.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388518);
INSERT INTO `fa_admin_log` VALUES (21, 1, 'admin', '/lollol.php/command/get_controller_list', '在线命令管理', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"OR \",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388519);
INSERT INTO `fa_admin_log` VALUES (22, 1, 'admin', '/lollol.php/command/command/action/execute', '在线命令管理 / 生成并执行命令', '{\"commandtype\":\"menu\",\"allcontroller\":\"0\",\"delete\":\"0\",\"force\":\"0\",\"controllerfile_text\":\"\",\"controllerfile\":\"Win.php\",\"action\":\"execute\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388521);
INSERT INTO `fa_admin_log` VALUES (23, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388600);
INSERT INTO `fa_admin_log` VALUES (24, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388600);
INSERT INTO `fa_admin_log` VALUES (25, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388600);
INSERT INTO `fa_admin_log` VALUES (26, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388600);
INSERT INTO `fa_admin_log` VALUES (27, 1, 'admin', '/lollol.php/win/add?dialog=1', '抽奖活动管理 / 添加', '{\"dialog\":\"1\",\"row\":{\"name\":\"测试\",\"images\":\"\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png\",\"json\":\"\",\"image\":\"\",\"views\":\"\",\"weigh\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719388605);
INSERT INTO `fa_admin_log` VALUES (28, 1, 'admin', '/lollol.php/win/add?dialog=1', '抽奖活动管理 / 添加', '{\"dialog\":\"1\",\"row\":{\"name\":\"\",\"images\":\"\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png\",\"json\":\"\",\"title_json\":\"[{&quot;params&quot;:&quot;超级大奖&quot;},{&quot;params&quot;:&quot;100&quot;},{&quot;params&quot;:&quot;50&quot;}]\",\"image\":\"\",\"views\":\"\",\"weigh\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719389815);
INSERT INTO `fa_admin_log` VALUES (29, 1, 'admin', '/lollol.php/win/add?dialog=1', '抽奖活动管理 / 添加', '{\"dialog\":\"1\",\"row\":{\"name\":\"是大师傅大是大非\",\"images\":\"\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png\",\"json\":\"\",\"image\":\"\",\"views\":\"\",\"weigh\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719390054);
INSERT INTO `fa_admin_log` VALUES (30, 1, 'admin', '/lollol.php/win/add?dialog=1', '抽奖活动管理 / 添加', '{\"dialog\":\"1\",\"row\":{\"name\":\"\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png\",\"json\":\"[{&quot;params&quot;:&quot;是是是&quot;},{&quot;params&quot;:&quot;是是是&quot;},{&quot;params&quot;:&quot;是是是&quot;}]\",\"image\":\"\",\"views\":\"\",\"weigh\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719390252);
INSERT INTO `fa_admin_log` VALUES (31, 1, 'admin', '/lollol.php/win/add?dialog=1', '抽奖活动管理 / 添加', '{\"dialog\":\"1\",\"row\":{\"name\":\"\",\"images\":\"\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png\",\"json\":\"{&quot;我&quot;:&quot;我&quot;}\",\"image\":\"\",\"views\":\"\",\"weigh\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719390303);
INSERT INTO `fa_admin_log` VALUES (32, 1, 'admin', '/lollol.php/win/add?dialog=1', '抽奖活动管理 / 添加', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png\",\"json\":\"[{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;}]\",\"image\":\"\",\"views\":\"\",\"weigh\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719390367);
INSERT INTO `fa_admin_log` VALUES (33, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;}]\",\"image\":\"\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719391016);
INSERT INTO `fa_admin_log` VALUES (34, 1, 'admin', '/lollol.php/win/add?dialog=1', '抽奖活动管理 / 添加', '{\"dialog\":\"1\",\"row\":{\"name\":\"是是是\",\"images\":\"\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"json\":\"[{&quot;name&quot;:&quot;是&quot;},{&quot;name&quot;:&quot;是&quot;}]\",\"image\":\"\",\"views\":\"\",\"weigh\":\"\"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719391171);
INSERT INTO `fa_admin_log` VALUES (35, 1, 'admin', '/lollol.php/win/edit/ids/7?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"是是是\",\"images\":\"\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"json\":\"[{&quot;name&quot;:&quot;是&quot;},{&quot;name&quot;:&quot;我&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"7\"},\"ids\":\"7\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719391256);
INSERT INTO `fa_admin_log` VALUES (36, 1, 'admin', '/lollol.php/win/edit/ids/7?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"是是是\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"json\":\"[{&quot;name&quot;:&quot;我&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"7\"},\"ids\":\"7\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719391270);
INSERT INTO `fa_admin_log` VALUES (37, 1, 'admin', '/lollol.php/auth/rule/edit/ids/102?dialog=1', '权限管理 / 菜单规则 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"win\",\"title\":\"抽奖活动管理\",\"url\":\"\",\"icon\":\"fa fa-circle-o\",\"condition\":\"\",\"menutype\":\"addtabs\",\"extend\":\"\",\"remark\":\"dsfdsfsd\",\"weigh\":\"0\",\"status\":\"normal\"},\"ids\":\"102\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392069);
INSERT INTO `fa_admin_log` VALUES (38, 1, 'admin', '/lollol.php/auth/rule/edit/ids/102?dialog=1', '权限管理 / 菜单规则 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"win\",\"title\":\"抽奖活动管理\",\"url\":\"\",\"icon\":\"fa fa-circle-o\",\"condition\":\"\",\"menutype\":\"addtabs\",\"extend\":\"\",\"remark\":\"只可添加6张图片，也就是单个活动只支持6个奖品，6个奖品中设置一个“谢谢参与奖”，奖品请直接拖动排序。请将“谢谢参与奖”一定排在第二个位置\",\"weigh\":\"0\",\"status\":\"normal\"},\"ids\":\"102\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392154);
INSERT INTO `fa_admin_log` VALUES (39, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392490);
INSERT INTO `fa_admin_log` VALUES (40, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392608);
INSERT INTO `fa_admin_log` VALUES (41, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392767);
INSERT INTO `fa_admin_log` VALUES (42, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392773);
INSERT INTO `fa_admin_log` VALUES (43, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392780);
INSERT INTO `fa_admin_log` VALUES (44, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;8&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392794);
INSERT INTO `fa_admin_log` VALUES (45, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392849);
INSERT INTO `fa_admin_log` VALUES (46, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392849);
INSERT INTO `fa_admin_log` VALUES (47, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392849);
INSERT INTO `fa_admin_log` VALUES (48, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392849);
INSERT INTO `fa_admin_log` VALUES (49, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392849);
INSERT INTO `fa_admin_log` VALUES (50, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392850);
INSERT INTO `fa_admin_log` VALUES (51, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392850);
INSERT INTO `fa_admin_log` VALUES (52, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392850);
INSERT INTO `fa_admin_log` VALUES (53, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"carousel\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719392850);
INSERT INTO `fa_admin_log` VALUES (54, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;8&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719393096);
INSERT INTO `fa_admin_log` VALUES (55, 0, 'Unknown', '/lollol.php/index/login', '', '{\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"ytyw\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719402017);
INSERT INTO `fa_admin_log` VALUES (56, 1, 'admin', '/lollol.php/index/login', '登录', '{\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"ebdy\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719402023);
INSERT INTO `fa_admin_log` VALUES (57, 1, 'admin', '/lollol.php/win/edit/ids/6?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"22222222\",\"images\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/47ed715cf5a6f247e6334577c814b430.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png,\\/uploads\\/20240626\\/34f96050cf3bc7c8d67daddb16ec423d.png,\\/uploads\\/20240626\\/67098325b9897a60d8cebee5fd31936a.png,\\/assets\\/img\\/qrcode.png,\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"json\":\"[{&quot;name&quot;:&quot;100&quot;},{&quot;name&quot;:&quot;超级大奖&quot;},{&quot;name&quot;:&quot;50&quot;},{&quot;name&quot;:&quot;20&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;s&quot;},{&quot;name&quot;:&quot;8&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/0544b273f08ff263f1a58d898d1c0ac1.png\",\"views\":\"0\",\"weigh\":\"6\"},\"ids\":\"6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719402029);
INSERT INTO `fa_admin_log` VALUES (58, 1, 'admin', '/lollol.php/auth/rule/edit/ids/102?dialog=1', '权限管理 / 菜单规则 / 编辑', '{\"dialog\":\"1\",\"__token__\":\"***\",\"row\":{\"ismenu\":\"1\",\"pid\":\"0\",\"name\":\"win\",\"title\":\"抽奖活动管理\",\"url\":\"\",\"icon\":\"fa fa-circle-o\",\"condition\":\"\",\"menutype\":\"addtabs\",\"extend\":\"\",\"remark\":\"只可添加6张图片，也就是单个活动只支持6个奖品，多出的图片会自动截断不显示。6个奖品中设置一个“谢谢参与奖”，奖品请直接拖动排序。请将“谢谢参与奖”一定排在第二个位置\",\"weigh\":\"0\",\"status\":\"normal\"},\"ids\":\"102\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719402560);
INSERT INTO `fa_admin_log` VALUES (59, 1, 'admin', '/lollol.php/win/del', '抽奖活动管理 / 删除', '{\"action\":\"del\",\"ids\":\"7,6\",\"params\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719403258);
INSERT INTO `fa_admin_log` VALUES (60, 1, 'admin', '/lollol.php/win/destroy', '抽奖活动管理 / 真实删除', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36', 1719403262);
INSERT INTO `fa_admin_log` VALUES (61, 1, 'admin', '/lollol.php/index/login', '登录', '{\"__token__\":\"***\",\"username\":\"admin\",\"password\":\"***\",\"captcha\":\"qtuq\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719457931);
INSERT INTO `fa_admin_log` VALUES (62, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719458886);
INSERT INTO `fa_admin_log` VALUES (63, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719458886);
INSERT INTO `fa_admin_log` VALUES (64, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719458886);
INSERT INTO `fa_admin_log` VALUES (65, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719458886);
INSERT INTO `fa_admin_log` VALUES (66, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719458886);
INSERT INTO `fa_admin_log` VALUES (67, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719458981);
INSERT INTO `fa_admin_log` VALUES (68, 1, 'admin', '/lollol.php/win/edit/ids/1?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"测试\",\"images\":\"\\/uploads\\/20240627\\/a8d8528514fcd398a59d87b2538f2b51.jpg,\\/uploads\\/20240627\\/f10aeea5cb1c56040d04d00f441b7bc9.png,\\/uploads\\/20240627\\/9102827f18d955ac21fda0142570cc62.jpg,\\/uploads\\/20240627\\/2621ef646bad734f5a2cd54fa4f7e418.jpg,\\/uploads\\/20240627\\/9260809b822aa8c080b78922a4aee743.jpg,\\/uploads\\/20240627\\/9a870e83dd89bfcfe2461bcf088e2fd9.jpg\",\"json\":\"[{&quot;name&quot;:&quot;iphone Vision Pro&quot;},{&quot;name&quot;:&quot;谢谢参与&quot;},{&quot;name&quot;:&quot;电动摩托&quot;},{&quot;name&quot;:&quot;iPhone手表&quot;},{&quot;name&quot;:&quot;NVIDIA RTX4090&quot;},{&quot;name&quot;:&quot;iphone15&quot;}]\",\"image\":\"\\/uploads\\/20240626\\/2fd04c3a36dec55b753a942156f069a5.png\",\"views\":\"0\",\"weigh\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719459095);
INSERT INTO `fa_admin_log` VALUES (69, 1, 'admin', '/lollol.php/ajax/upload', '', '{\"category\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719459183);
INSERT INTO `fa_admin_log` VALUES (70, 1, 'admin', '/lollol.php/win/edit/ids/1?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"测试\",\"images\":\"\\/uploads\\/20240627\\/a8d8528514fcd398a59d87b2538f2b51.jpg,\\/uploads\\/20240627\\/f10aeea5cb1c56040d04d00f441b7bc9.png,\\/uploads\\/20240627\\/9102827f18d955ac21fda0142570cc62.jpg,\\/uploads\\/20240627\\/2621ef646bad734f5a2cd54fa4f7e418.jpg,\\/uploads\\/20240627\\/9260809b822aa8c080b78922a4aee743.jpg,\\/uploads\\/20240627\\/9a870e83dd89bfcfe2461bcf088e2fd9.jpg\",\"json\":\"[{&quot;name&quot;:&quot;iphone Vision Pro&quot;},{&quot;name&quot;:&quot;谢谢参与&quot;},{&quot;name&quot;:&quot;电动摩托&quot;},{&quot;name&quot;:&quot;iPhone手表&quot;},{&quot;name&quot;:&quot;NVIDIA RTX4090&quot;},{&quot;name&quot;:&quot;iphone15&quot;}]\",\"image\":\"\\/uploads\\/20240627\\/ef6965e014419e8145fd8f37d43e176f.png\",\"views\":\"0\",\"weigh\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719459184);
INSERT INTO `fa_admin_log` VALUES (71, 1, 'admin', '/lollol.php/win/edit/ids/1?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"测试\",\"images\":\"\\/uploads\\/20240627\\/a8d8528514fcd398a59d87b2538f2b51.jpg,\\/uploads\\/20240627\\/f10aeea5cb1c56040d04d00f441b7bc9.png,\\/uploads\\/20240627\\/9102827f18d955ac21fda0142570cc62.jpg,\\/uploads\\/20240627\\/2621ef646bad734f5a2cd54fa4f7e418.jpg,\\/uploads\\/20240627\\/9260809b822aa8c080b78922a4aee743.jpg,\\/uploads\\/20240627\\/9a870e83dd89bfcfe2461bcf088e2fd9.jpg\",\"json\":\"[{&quot;name&quot;:&quot;iphone Vision Pro&quot;},{&quot;name&quot;:&quot;谢谢参与&quot;},{&quot;name&quot;:&quot;电动摩托&quot;},{&quot;name&quot;:&quot;iPhone手表&quot;},{&quot;name&quot;:&quot;NVIDIA RTX4090&quot;},{&quot;name&quot;:&quot;iphone15&quot;}]\",\"image\":\"\\/uploads\\/20240627\\/ef6965e014419e8145fd8f37d43e176f.png\",\"views\":\"0\",\"weigh\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Mobile Safari/537.36', 1719459692);
INSERT INTO `fa_admin_log` VALUES (72, 1, 'admin', '/lollol.php/win/edit/ids/1?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"测试\",\"images\":\"\\/uploads\\/20240627\\/a8d8528514fcd398a59d87b2538f2b51.jpg,\\/uploads\\/20240627\\/9102827f18d955ac21fda0142570cc62.jpg,\\/uploads\\/20240627\\/2621ef646bad734f5a2cd54fa4f7e418.jpg,\\/uploads\\/20240627\\/9260809b822aa8c080b78922a4aee743.jpg,\\/uploads\\/20240627\\/f10aeea5cb1c56040d04d00f441b7bc9.png,\\/uploads\\/20240627\\/9a870e83dd89bfcfe2461bcf088e2fd9.jpg\",\"json\":\"[{&quot;name&quot;:&quot;iphone Vision Pro&quot;},{&quot;name&quot;:&quot;电动摩托&quot;},{&quot;name&quot;:&quot;iPhone手表&quot;},{&quot;name&quot;:&quot;NVIDIA RTX4090&quot;},{&quot;name&quot;:&quot;谢谢参与&quot;},{&quot;name&quot;:&quot;iphone15&quot;}]\",\"image\":\"\\/uploads\\/20240627\\/ef6965e014419e8145fd8f37d43e176f.png\",\"views\":\"0\",\"weigh\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719471035);
INSERT INTO `fa_admin_log` VALUES (73, 1, 'admin', '/lollol.php/win/edit/ids/1?dialog=1', '抽奖活动管理 / 编辑', '{\"dialog\":\"1\",\"row\":{\"name\":\"测试\",\"images\":\"\\/uploads\\/20240627\\/a8d8528514fcd398a59d87b2538f2b51.jpg,\\/uploads\\/20240627\\/9102827f18d955ac21fda0142570cc62.jpg,\\/uploads\\/20240627\\/2621ef646bad734f5a2cd54fa4f7e418.jpg,\\/uploads\\/20240627\\/9260809b822aa8c080b78922a4aee743.jpg,\\/uploads\\/20240627\\/9a870e83dd89bfcfe2461bcf088e2fd9.jpg,\\/uploads\\/20240627\\/f10aeea5cb1c56040d04d00f441b7bc9.png\",\"json\":\"[{&quot;name&quot;:&quot;iphone Vision Pro&quot;},{&quot;name&quot;:&quot;电动摩托&quot;},{&quot;name&quot;:&quot;iPhone手表&quot;},{&quot;name&quot;:&quot;NVIDIA RTX4090&quot;},{&quot;name&quot;:&quot;iphone15&quot;},{&quot;name&quot;:&quot;谢谢参与&quot;}]\",\"image\":\"\\/uploads\\/20240627\\/ef6965e014419e8145fd8f37d43e176f.png\",\"views\":\"0\",\"weigh\":\"1\"},\"ids\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 1719471099);

-- ----------------------------
-- Table structure for fa_area
-- ----------------------------
DROP TABLE IF EXISTS `fa_area`;
CREATE TABLE `fa_area`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) NULL DEFAULT NULL COMMENT '父id',
  `shortname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '简称',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `mergename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '全称',
  `level` tinyint(4) NULL DEFAULT NULL COMMENT '层级:1=省,2=市,3=区/县',
  `pinyin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '拼音',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '长途区号',
  `zip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮编',
  `first` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '首字母',
  `lng` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '地区表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_area
-- ----------------------------

-- ----------------------------
-- Table structure for fa_attachment
-- ----------------------------
DROP TABLE IF EXISTS `fa_attachment`;
CREATE TABLE `fa_attachment`  (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类别',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片帧数',
  `filename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件名称',
  `filesize` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `mimetype` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '透传数据',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建日期',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `uploadtime` bigint(16) NULL DEFAULT NULL COMMENT '上传时间',
  `storage` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件 sha1编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_attachment
-- ----------------------------
INSERT INTO `fa_attachment` VALUES (1, '', 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 'qrcode.png', 21859, 'image/png', '', 1491635035, 1491635035, 1491635035, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6');
INSERT INTO `fa_attachment` VALUES (2, '', 1, 0, '/uploads/20240626/67098325b9897a60d8cebee5fd31936a.png', '54', '67', 'png', 0, '20.png', 1149, 'image/png', '', 1719388600, 1719388600, 1719388600, 'local', '6ddcc6db22c87ff17d166739238c2e41800f18e1');
INSERT INTO `fa_attachment` VALUES (3, '', 1, 0, '/uploads/20240626/34f96050cf3bc7c8d67daddb16ec423d.png', '54', '66', 'png', 0, '50.png', 1227, 'image/png', '', 1719388600, 1719388600, 1719388600, 'local', '4c1df9c304761f82affa516ccd24287023033fe3');
INSERT INTO `fa_attachment` VALUES (4, '', 1, 0, '/uploads/20240626/0544b273f08ff263f1a58d898d1c0ac1.png', '56', '67', 'png', 0, '100.png', 1147, 'image/png', '', 1719388600, 1719388600, 1719388600, 'local', '40962bab6132720a2dda3142a8b7ab573d3f5084');
INSERT INTO `fa_attachment` VALUES (5, '', 1, 0, '/uploads/20240626/47ed715cf5a6f247e6334577c814b430.png', '69', '73', 'png', 0, 'gift.png', 1312, 'image/png', '', 1719388600, 1719388600, 1719388600, 'local', '905d5c7e051172dd8a645eb58e49e6484e64d50c');
INSERT INTO `fa_attachment` VALUES (6, '', 1, 0, '/uploads/20240626/0d93a1008bb452fbc60887711f6519f0.png', '750', '1334', 'png', 0, 'bg.png', 87070, 'image/png', '', 1719392849, 1719392849, 1719392849, 'local', '0772a26a7a76a6246e88ff0d51875eee8f9b8b51');
INSERT INTO `fa_attachment` VALUES (7, '', 1, 0, '/uploads/20240626/a2e3077eb934d60ea494aa4f941e5618.png', '380', '63', 'png', 0, 'border.png', 865, 'image/png', '', 1719392849, 1719392849, 1719392849, 'local', '359b2ce1b31d17299e2b2520ce878313ce8c40fd');
INSERT INTO `fa_attachment` VALUES (8, '', 1, 0, '/uploads/20240626/079efbd8ca81e0184c70562c6468d33e.png', '160', '198', 'png', 0, 'button.png', 4211, 'image/png', '', 1719392850, 1719392850, 1719392850, 'local', '409cf4f8d08029023f17a39e94752023135ed890');
INSERT INTO `fa_attachment` VALUES (9, '', 1, 0, '/uploads/20240626/8005f73c239d51b475dfcc2b3195478b.png', '750', '1024', 'png', 0, 'caidai.png', 15464, 'image/png', '', 1719392850, 1719392850, 1719392850, 'local', '6cbdb1246cbf4ae8f717840d35acaf8e4d00d55a');
INSERT INTO `fa_attachment` VALUES (10, '', 1, 0, '/uploads/20240626/2fd04c3a36dec55b753a942156f069a5.png', '665', '665', 'png', 0, 'circle.png', 16401, 'image/png', '', 1719392850, 1719392850, 1719392850, 'local', 'c0caf27c2d48e4295fa992ded85b01bafaf9d6c0');
INSERT INTO `fa_attachment` VALUES (11, '', 1, 0, '/uploads/20240626/2da16b87b2bcb6b37404d60c51d8ec07.png', '357', '22', 'png', 0, 'list.png', 984, 'image/png', '', 1719392850, 1719392850, 1719392850, 'local', 'cc8315b9dd6a2044e1c4a21bc24618f82e9ace86');
INSERT INTO `fa_attachment` VALUES (12, '', 1, 0, '/uploads/20240627/9a870e83dd89bfcfe2461bcf088e2fd9.jpg', '1242', '1235', 'jpg', 0, '微信图片_20240627112724.jpg', 393758, 'image/jpeg', '', 1719458886, 1719458886, 1719458886, 'local', 'f1523b730f94a68aeee86bc6a5c9fac55e03b326');
INSERT INTO `fa_attachment` VALUES (13, '', 1, 0, '/uploads/20240627/9260809b822aa8c080b78922a4aee743.jpg', '1242', '1225', 'jpg', 0, '微信图片_20240627112725.jpg', 339970, 'image/jpeg', '', 1719458886, 1719458886, 1719458886, 'local', '176aeb766f0ff1e8b02b0bd781543198ad659668');
INSERT INTO `fa_attachment` VALUES (14, '', 1, 0, '/uploads/20240627/2621ef646bad734f5a2cd54fa4f7e418.jpg', '1242', '1235', 'jpg', 0, '微信图片_20240627112726.jpg', 290184, 'image/jpeg', '', 1719458886, 1719458886, 1719458886, 'local', '714117df151ea34d6514b542f948a58d1311f595');
INSERT INTO `fa_attachment` VALUES (15, '', 1, 0, '/uploads/20240627/9102827f18d955ac21fda0142570cc62.jpg', '1242', '1237', 'jpg', 0, '微信图片_20240627112727.jpg', 842580, 'image/jpeg', '', 1719458886, 1719458886, 1719458886, 'local', '514179f26732f3246450a96fb2c491a2230b46f3');
INSERT INTO `fa_attachment` VALUES (16, '', 1, 0, '/uploads/20240627/a8d8528514fcd398a59d87b2538f2b51.jpg', '1242', '1219', 'jpg', 0, '微信图片_202406271127271.jpg', 283053, 'image/jpeg', '', 1719458886, 1719458886, 1719458886, 'local', '269d179f03db325756be50fa2da6df036920688c');
INSERT INTO `fa_attachment` VALUES (17, '', 1, 0, '/uploads/20240627/f10aeea5cb1c56040d04d00f441b7bc9.png', '77', '77', 'png', 0, 'face.png', 1419, 'image/png', '', 1719458981, 1719458981, 1719458981, 'local', 'b2fc3f038f3aab5e264e95337d25fd465f036f0c');
INSERT INTO `fa_attachment` VALUES (18, '', 1, 0, '/uploads/20240627/ef6965e014419e8145fd8f37d43e176f.png', '400', '400', 'png', 0, '抽奖测试二维码.png', 2098, 'image/png', '', 1719459183, 1719459183, 1719459183, 'local', '0191302aad866579ae202aadd66bfb5d177a8beb');

-- ----------------------------
-- Table structure for fa_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `fa_auth_group`;
CREATE TABLE `fa_auth_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父组别',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '组名',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规则ID',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_auth_group
-- ----------------------------
INSERT INTO `fa_auth_group` VALUES (1, 0, 'Admin group', '*', 1491635035, 1491635035, 'normal');
INSERT INTO `fa_auth_group` VALUES (2, 1, 'Second group', '13,14,16,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,1,9,10,11,7,6,8,2,4,5', 1491635035, 1491635035, 'normal');
INSERT INTO `fa_auth_group` VALUES (3, 2, 'Third group', '1,4,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,5', 1491635035, 1491635035, 'normal');
INSERT INTO `fa_auth_group` VALUES (4, 1, 'Second group 2', '1,4,13,14,15,16,17,55,56,57,58,59,60,61,62,63,64,65', 1491635035, 1491635035, 'normal');
INSERT INTO `fa_auth_group` VALUES (5, 2, 'Third group 2', '1,2,6,7,8,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34', 1491635035, 1491635035, 'normal');

-- ----------------------------
-- Table structure for fa_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `fa_auth_group_access`;
CREATE TABLE `fa_auth_group_access`  (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '会员ID',
  `group_id` int(10) UNSIGNED NOT NULL COMMENT '级别ID',
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限分组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_auth_group_access
-- ----------------------------
INSERT INTO `fa_auth_group_access` VALUES (1, 1);

-- ----------------------------
-- Table structure for fa_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `fa_auth_rule`;
CREATE TABLE `fa_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '图标',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则URL',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为菜单',
  `menutype` enum('addtabs','blank','dialog','ajax') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单类型',
  `extend` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '扩展属性',
  `py` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '拼音首字母',
  `pinyin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '拼音',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `weigh`(`weigh`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 111 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '节点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_auth_rule
-- ----------------------------
INSERT INTO `fa_auth_rule` VALUES (1, 'file', 0, 'dashboard', 'Dashboard', 'fa fa-dashboard', '', '', 'Dashboard tips', 1, NULL, '', 'kzt', 'kongzhitai', 1491635035, 1491635035, 143, 'normal');
INSERT INTO `fa_auth_rule` VALUES (2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', '', 1, NULL, '', 'cggl', 'changguiguanli', 1491635035, 1491635035, 137, 'normal');
INSERT INTO `fa_auth_rule` VALUES (3, 'file', 0, 'category', 'Category', 'fa fa-leaf', '', '', 'Category tips', 0, NULL, '', 'flgl', 'fenleiguanli', 1491635035, 1491635035, 119, 'normal');
INSERT INTO `fa_auth_rule` VALUES (4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', '', 'Addon tips', 1, NULL, '', 'cjgl', 'chajianguanli', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', '', 1, NULL, '', 'qxgl', 'quanxianguanli', 1491635035, 1491635035, 99, 'normal');
INSERT INTO `fa_auth_rule` VALUES (6, 'file', 2, 'general/config', 'Config', 'fa fa-cog', '', '', 'Config tips', 1, NULL, '', 'xtpz', 'xitongpeizhi', 1491635035, 1491635035, 60, 'normal');
INSERT INTO `fa_auth_rule` VALUES (7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', '', 'Attachment tips', 1, NULL, '', 'fjgl', 'fujianguanli', 1491635035, 1491635035, 53, 'normal');
INSERT INTO `fa_auth_rule` VALUES (8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', '', 1, NULL, '', 'grzl', 'gerenziliao', 1491635035, 1491635035, 34, 'normal');
INSERT INTO `fa_auth_rule` VALUES (9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', '', 'Admin tips', 1, NULL, '', 'glygl', 'guanliyuanguanli', 1491635035, 1491635035, 118, 'normal');
INSERT INTO `fa_auth_rule` VALUES (10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', '', 'Admin log tips', 1, NULL, '', 'glyrz', 'guanliyuanrizhi', 1491635035, 1491635035, 113, 'normal');
INSERT INTO `fa_auth_rule` VALUES (11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', '', 'Group tips', 1, NULL, '', 'jsz', 'juesezu', 1491635035, 1491635035, 109, 'normal');
INSERT INTO `fa_auth_rule` VALUES (12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', '', 'Rule tips', 1, NULL, '', 'cdgz', 'caidanguize', 1491635035, 1491635035, 104, 'normal');
INSERT INTO `fa_auth_rule` VALUES (13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 136, 'normal');
INSERT INTO `fa_auth_rule` VALUES (14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 135, 'normal');
INSERT INTO `fa_auth_rule` VALUES (15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 133, 'normal');
INSERT INTO `fa_auth_rule` VALUES (16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 134, 'normal');
INSERT INTO `fa_auth_rule` VALUES (17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 132, 'normal');
INSERT INTO `fa_auth_rule` VALUES (18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 52, 'normal');
INSERT INTO `fa_auth_rule` VALUES (19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 51, 'normal');
INSERT INTO `fa_auth_rule` VALUES (20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 50, 'normal');
INSERT INTO `fa_auth_rule` VALUES (21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 49, 'normal');
INSERT INTO `fa_auth_rule` VALUES (22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 48, 'normal');
INSERT INTO `fa_auth_rule` VALUES (23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', '', 'Attachment tips', 0, NULL, '', '', '', 1491635035, 1491635035, 59, 'normal');
INSERT INTO `fa_auth_rule` VALUES (24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 58, 'normal');
INSERT INTO `fa_auth_rule` VALUES (25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 57, 'normal');
INSERT INTO `fa_auth_rule` VALUES (26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 56, 'normal');
INSERT INTO `fa_auth_rule` VALUES (27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 55, 'normal');
INSERT INTO `fa_auth_rule` VALUES (28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 54, 'normal');
INSERT INTO `fa_auth_rule` VALUES (29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 33, 'normal');
INSERT INTO `fa_auth_rule` VALUES (30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 32, 'normal');
INSERT INTO `fa_auth_rule` VALUES (31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 31, 'normal');
INSERT INTO `fa_auth_rule` VALUES (32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 30, 'normal');
INSERT INTO `fa_auth_rule` VALUES (33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 29, 'normal');
INSERT INTO `fa_auth_rule` VALUES (34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 28, 'normal');
INSERT INTO `fa_auth_rule` VALUES (35, 'file', 3, 'category/index', 'View', 'fa fa-circle-o', '', '', 'Category tips', 0, NULL, '', '', '', 1491635035, 1491635035, 142, 'normal');
INSERT INTO `fa_auth_rule` VALUES (36, 'file', 3, 'category/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 141, 'normal');
INSERT INTO `fa_auth_rule` VALUES (37, 'file', 3, 'category/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 140, 'normal');
INSERT INTO `fa_auth_rule` VALUES (38, 'file', 3, 'category/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 139, 'normal');
INSERT INTO `fa_auth_rule` VALUES (39, 'file', 3, 'category/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 138, 'normal');
INSERT INTO `fa_auth_rule` VALUES (40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', '', 'Admin tips', 0, NULL, '', '', '', 1491635035, 1491635035, 117, 'normal');
INSERT INTO `fa_auth_rule` VALUES (41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 116, 'normal');
INSERT INTO `fa_auth_rule` VALUES (42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 115, 'normal');
INSERT INTO `fa_auth_rule` VALUES (43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 114, 'normal');
INSERT INTO `fa_auth_rule` VALUES (44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', '', 'Admin log tips', 0, NULL, '', '', '', 1491635035, 1491635035, 112, 'normal');
INSERT INTO `fa_auth_rule` VALUES (45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 111, 'normal');
INSERT INTO `fa_auth_rule` VALUES (46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 110, 'normal');
INSERT INTO `fa_auth_rule` VALUES (47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', '', 'Group tips', 0, NULL, '', '', '', 1491635035, 1491635035, 108, 'normal');
INSERT INTO `fa_auth_rule` VALUES (48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 107, 'normal');
INSERT INTO `fa_auth_rule` VALUES (49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 106, 'normal');
INSERT INTO `fa_auth_rule` VALUES (50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 105, 'normal');
INSERT INTO `fa_auth_rule` VALUES (51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', '', 'Rule tips', 0, NULL, '', '', '', 1491635035, 1491635035, 103, 'normal');
INSERT INTO `fa_auth_rule` VALUES (52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 102, 'normal');
INSERT INTO `fa_auth_rule` VALUES (53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 101, 'normal');
INSERT INTO `fa_auth_rule` VALUES (54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 100, 'normal');
INSERT INTO `fa_auth_rule` VALUES (55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', '', 'Addon tips', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (59, 'file', 4, 'addon/downloaded', 'Local addon', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (66, 'file', 0, 'user', 'User', 'fa fa-user-circle', '', '', '', 1, NULL, '', 'hygl', 'huiyuanguanli', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', '', 1, NULL, '', 'hygl', 'huiyuanguanli', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', '', 1, NULL, '', 'hyfz', 'huiyuanfenzu', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', '', 1, NULL, '', 'hygz', 'huiyuanguize', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (85, 'file', 0, 'command', '在线命令管理', 'fa fa-terminal', '', '', '', 1, NULL, '', 'zxmlgl', 'zaixianminglingguanli', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (86, 'file', 85, 'command/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'zk', 'zhakan', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (87, 'file', 85, 'command/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'tj', 'tianjia', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (88, 'file', 85, 'command/detail', '详情', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'xq', 'xiangqing', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (89, 'file', 85, 'command/command', '生成并执行命令', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'scbzxml', 'shengchengbingzhixingmingling', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (90, 'file', 85, 'command/execute', '再次执行命令', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'zczxml', 'zaicizhixingmingling', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (91, 'file', 85, 'command/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'sc', 'shanchu', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (92, 'file', 85, 'command/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'plgx', 'pilianggengxin', 1719320222, 1719320222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (93, 'file', 0, 'test', '测试管理', 'fa fa-circle-o', '', '', '', 1, NULL, '', 'csgl', 'ceshiguanli', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (94, 'file', 93, 'test/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'zk', 'zhakan', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (95, 'file', 93, 'test/recyclebin', '回收站', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'hsz', 'huishouzhan', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (96, 'file', 93, 'test/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'tj', 'tianjia', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (97, 'file', 93, 'test/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'bj', 'bianji', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (98, 'file', 93, 'test/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'sc', 'shanchu', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (99, 'file', 93, 'test/destroy', '真实删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'zssc', 'zhenshishanchu', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (100, 'file', 93, 'test/restore', '还原', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'hy', 'huanyuan', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (101, 'file', 93, 'test/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'plgx', 'pilianggengxin', 1719320969, 1719320969, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (102, 'file', 0, 'win', '抽奖活动管理', 'fa fa-circle-o', '', '', '只可添加6张图片，也就是单个活动只支持6个奖品，多出的图片会自动截断不显示。6个奖品中设置一个“谢谢参与奖”，奖品请直接拖动排序。请将“谢谢参与奖”一定排在第二个位置', 1, 'addtabs', '', 'cjhdgl', 'choujianghuodongguanli', 1719388521, 1719402560, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (103, 'file', 102, 'win/index', '查看', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'zk', 'zhakan', 1719388521, 1719388521, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (104, 'file', 102, 'win/recyclebin', '回收站', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'hsz', 'huishouzhan', 1719388521, 1719388521, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (105, 'file', 102, 'win/add', '添加', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'tj', 'tianjia', 1719388521, 1719388521, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (106, 'file', 102, 'win/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'bj', 'bianji', 1719388521, 1719388521, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (107, 'file', 102, 'win/del', '删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'sc', 'shanchu', 1719388521, 1719388521, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (108, 'file', 102, 'win/destroy', '真实删除', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'zssc', 'zhenshishanchu', 1719388521, 1719388521, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (109, 'file', 102, 'win/restore', '还原', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'hy', 'huanyuan', 1719388521, 1719388521, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (110, 'file', 102, 'win/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, NULL, '', 'plgx', 'pilianggengxin', 1719388521, 1719388521, 0, 'normal');

-- ----------------------------
-- Table structure for fa_category
-- ----------------------------
DROP TABLE IF EXISTS `fa_category`;
CREATE TABLE `fa_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '栏目类型',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `flag` set('hot','index','recommend') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '图片',
  `keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '描述',
  `diyname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '自定义名称',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `weigh`(`weigh`, `id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_category
-- ----------------------------
INSERT INTO `fa_category` VALUES (1, 0, 'page', '官方新闻', 'news', 'recommend', '/assets/img/qrcode.png', '', '', 'news', 1491635035, 1491635035, 1, 'normal');
INSERT INTO `fa_category` VALUES (2, 0, 'page', '移动应用', 'mobileapp', 'hot', '/assets/img/qrcode.png', '', '', 'mobileapp', 1491635035, 1491635035, 2, 'normal');
INSERT INTO `fa_category` VALUES (3, 2, 'page', '微信公众号', 'wechatpublic', 'index', '/assets/img/qrcode.png', '', '', 'wechatpublic', 1491635035, 1491635035, 3, 'normal');
INSERT INTO `fa_category` VALUES (4, 2, 'page', 'Android开发', 'android', 'recommend', '/assets/img/qrcode.png', '', '', 'android', 1491635035, 1491635035, 4, 'normal');
INSERT INTO `fa_category` VALUES (5, 0, 'page', '软件产品', 'software', 'recommend', '/assets/img/qrcode.png', '', '', 'software', 1491635035, 1491635035, 5, 'normal');
INSERT INTO `fa_category` VALUES (6, 5, 'page', '网站建站', 'website', 'recommend', '/assets/img/qrcode.png', '', '', 'website', 1491635035, 1491635035, 6, 'normal');
INSERT INTO `fa_category` VALUES (7, 5, 'page', '企业管理软件', 'company', 'index', '/assets/img/qrcode.png', '', '', 'company', 1491635035, 1491635035, 7, 'normal');
INSERT INTO `fa_category` VALUES (8, 6, 'page', 'PC端', 'website-pc', 'recommend', '/assets/img/qrcode.png', '', '', 'website-pc', 1491635035, 1491635035, 8, 'normal');
INSERT INTO `fa_category` VALUES (9, 6, 'page', '移动端', 'website-mobile', 'recommend', '/assets/img/qrcode.png', '', '', 'website-mobile', 1491635035, 1491635035, 9, 'normal');
INSERT INTO `fa_category` VALUES (10, 7, 'page', 'CRM系统 ', 'company-crm', 'recommend', '/assets/img/qrcode.png', '', '', 'company-crm', 1491635035, 1491635035, 10, 'normal');
INSERT INTO `fa_category` VALUES (11, 7, 'page', 'SASS平台软件', 'company-sass', 'recommend', '/assets/img/qrcode.png', '', '', 'company-sass', 1491635035, 1491635035, 11, 'normal');
INSERT INTO `fa_category` VALUES (12, 0, 'test', '测试1', 'test1', 'recommend', '/assets/img/qrcode.png', '', '', 'test1', 1491635035, 1491635035, 12, 'normal');
INSERT INTO `fa_category` VALUES (13, 0, 'test', '测试2', 'test2', 'recommend', '/assets/img/qrcode.png', '', '', 'test2', 1491635035, 1491635035, 13, 'normal');

-- ----------------------------
-- Table structure for fa_command
-- ----------------------------
DROP TABLE IF EXISTS `fa_command`;
CREATE TABLE `fa_command`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型',
  `params` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '参数',
  `command` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '命令',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回结果',
  `executetime` bigint(16) UNSIGNED NULL DEFAULT NULL COMMENT '执行时间',
  `createtime` bigint(16) UNSIGNED NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  `status` enum('successed','failured') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'failured' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线命令表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_command
-- ----------------------------
INSERT INTO `fa_command` VALUES (1, 'crud', '[\"--table=fa_test\"]', 'php think crud --table=fa_test', 'Build Successed', 1719320959, 1719320959, 1719320959, 'successed');
INSERT INTO `fa_command` VALUES (2, 'menu', '[\"--controller=Test\"]', 'php think menu --controller=Test', 'Build Successed!', 1719320969, 1719320969, 1719320969, 'successed');
INSERT INTO `fa_command` VALUES (3, 'crud', '[\"--table=fa_win\"]', 'php think crud --table=fa_win', 'Build Successed', 1719388511, 1719388511, 1719388511, 'successed');
INSERT INTO `fa_command` VALUES (4, 'menu', '[]', 'php think menu ', 'please input controller name', 1719388514, 1719388514, 1719388514, 'failured');
INSERT INTO `fa_command` VALUES (5, 'menu', '[\"--controller=Win\"]', 'php think menu --controller=Win', 'Build Successed!', 1719388521, 1719388521, 1719388521, 'successed');

-- ----------------------------
-- Table structure for fa_config
-- ----------------------------
DROP TABLE IF EXISTS `fa_config`;
CREATE TABLE `fa_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分组',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `visible` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '可见条件',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '变量值',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '变量字典数据',
  `rule` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '扩展属性',
  `setting` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '配置',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_config
-- ----------------------------
INSERT INTO `fa_config` VALUES (1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', '', '我的网站', '', 'required', '', '');
INSERT INTO `fa_config` VALUES (2, 'beian', 'basic', 'Beian', '粤ICP备15000000号-1', 'string', '', '', '', '', '', '');
INSERT INTO `fa_config` VALUES (3, 'cdnurl', 'basic', 'Cdn url', '如果全站静态资源使用第三方云储存请配置该值', 'string', '', '', '', '', '', '');
INSERT INTO `fa_config` VALUES (4, 'version', 'basic', 'Version', '如果静态资源有变动请重新配置该值', 'string', '', '1.0.1', '', 'required', '', '');
INSERT INTO `fa_config` VALUES (5, 'timezone', 'basic', 'Timezone', '', 'string', '', 'Asia/Shanghai', '', 'required', '', '');
INSERT INTO `fa_config` VALUES (6, 'forbiddenip', 'basic', 'Forbidden ip', '一行一条记录', 'text', '', '', '', '', '', '');
INSERT INTO `fa_config` VALUES (7, 'languages', 'basic', 'Languages', '', 'array', '', '{\"backend\":\"zh-cn\",\"frontend\":\"zh-cn\"}', '', 'required', '', '');
INSERT INTO `fa_config` VALUES (8, 'fixedpage', 'basic', 'Fixed page', '请输入左侧菜单栏存在的链接', 'string', '', 'dashboard', '', 'required', '', '');
INSERT INTO `fa_config` VALUES (9, 'categorytype', 'dictionary', 'Category type', '', 'array', '', '{\"default\":\"Default\",\"page\":\"Page\",\"article\":\"Article\",\"test\":\"Test\"}', '', '', '', '');
INSERT INTO `fa_config` VALUES (10, 'configgroup', 'dictionary', 'Config group', '', 'array', '', '{\"basic\":\"Basic\",\"email\":\"Email\",\"dictionary\":\"Dictionary\",\"user\":\"User\",\"example\":\"Example\"}', '', '', '', '');
INSERT INTO `fa_config` VALUES (11, 'mail_type', 'email', 'Mail type', '选择邮件发送方式', 'select', '', '1', '[\"请选择\",\"SMTP\"]', '', '', '');
INSERT INTO `fa_config` VALUES (12, 'mail_smtp_host', 'email', 'Mail smtp host', '错误的配置发送邮件会导致服务器超时', 'string', '', 'smtp.qq.com', '', '', '', '');
INSERT INTO `fa_config` VALUES (13, 'mail_smtp_port', 'email', 'Mail smtp port', '(不加密默认25,SSL默认465,TLS默认587)', 'string', '', '465', '', '', '', '');
INSERT INTO `fa_config` VALUES (14, 'mail_smtp_user', 'email', 'Mail smtp user', '（填写完整用户名）', 'string', '', '', '', '', '', '');
INSERT INTO `fa_config` VALUES (15, 'mail_smtp_pass', 'email', 'Mail smtp password', '（填写您的密码或授权码）', 'password', '', '', '', '', '', '');
INSERT INTO `fa_config` VALUES (16, 'mail_verify_type', 'email', 'Mail vertify type', '（SMTP验证方式[推荐SSL]）', 'select', '', '2', '[\"无\",\"TLS\",\"SSL\"]', '', '', '');
INSERT INTO `fa_config` VALUES (17, 'mail_from', 'email', 'Mail from', '', 'string', '', '', '', '', '', '');
INSERT INTO `fa_config` VALUES (18, 'attachmentcategory', 'dictionary', 'Attachment category', '', 'array', '', '{\"category1\":\"Category1\",\"category2\":\"Category2\",\"custom\":\"Custom\"}', '', '', '', '');

-- ----------------------------
-- Table structure for fa_ems
-- ----------------------------
DROP TABLE IF EXISTS `fa_ems`;
CREATE TABLE `fa_ems`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '事件',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证次数',
  `ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'IP',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '邮箱验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_ems
-- ----------------------------

-- ----------------------------
-- Table structure for fa_sms
-- ----------------------------
DROP TABLE IF EXISTS `fa_sms`;
CREATE TABLE `fa_sms`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号',
  `code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证次数',
  `ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'IP',
  `createtime` bigint(16) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '短信验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_sms
-- ----------------------------

-- ----------------------------
-- Table structure for fa_test
-- ----------------------------
DROP TABLE IF EXISTS `fa_test`;
CREATE TABLE `fa_test`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) NULL DEFAULT 0 COMMENT '会员ID',
  `admin_id` int(10) NULL DEFAULT 0 COMMENT '管理员ID',
  `category_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '分类ID(单选)',
  `category_ids` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类ID(多选)',
  `tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标签',
  `week` enum('monday','tuesday','wednesday') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
  `flag` set('hot','index','recommend') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
  `genderdata` enum('male','female') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
  `hobbydata` set('music','reading','swimming') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '图片',
  `images` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '图片组',
  `attachfile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '附件',
  `keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '描述',
  `city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '省市',
  `array` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '数组:value=值',
  `json` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '配置:key=名称,value=值',
  `multiplejson` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '二维数组:title=标题,intro=介绍,author=作者,age=年龄',
  `price` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '价格',
  `views` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '点击',
  `workrange` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '时间区间',
  `startdate` date NULL DEFAULT NULL COMMENT '开始日期',
  `activitytime` datetime(0) NULL DEFAULT NULL COMMENT '活动时间(datetime)',
  `year` year NULL DEFAULT NULL COMMENT '年',
  `times` time(0) NULL DEFAULT NULL COMMENT '时间',
  `refreshtime` bigint(16) NULL DEFAULT NULL COMMENT '刷新时间',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `deletetime` bigint(16) NULL DEFAULT NULL COMMENT '删除时间',
  `weigh` int(10) NULL DEFAULT 0 COMMENT '权重',
  `switch` tinyint(1) NULL DEFAULT 0 COMMENT '开关',
  `status` enum('normal','hidden') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'normal' COMMENT '状态',
  `state` enum('0','1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '测试表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_test
-- ----------------------------
INSERT INTO `fa_test` VALUES (1, 1, 1, 12, '12,13', '互联网,计算机', 'monday', 'hot,index', 'male', 'music,reading', '我是一篇测试文章', '<p>我是测试内容</p>', '/assets/img/avatar.png', '/assets/img/avatar.png,/assets/img/qrcode.png', '/assets/img/avatar.png', '关键字', '我是一篇测试文章描述，内容过多时将自动隐藏', '广西壮族自治区/百色市/平果县', '[\"a\",\"b\"]', '{\"a\":\"1\",\"b\":\"2\"}', '[{\"title\":\"标题一\",\"intro\":\"介绍一\",\"author\":\"小明\",\"age\":\"21\"}]', 0.00, 0, '2020-10-01 00:00:00 - 2021-10-31 23:59:59', '2017-07-10', '2017-07-10 18:24:45', 2017, '18:24:45', 1491635035, 1491635035, 1491635035, NULL, 0, 1, 'normal', '1');

-- ----------------------------
-- Table structure for fa_user
-- ----------------------------
DROP TABLE IF EXISTS `fa_user`;
CREATE TABLE `fa_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '组别ID',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像',
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '性别',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `bio` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '格言',
  `money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '余额',
  `score` int(10) NOT NULL DEFAULT 0 COMMENT '积分',
  `successions` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '连续登录天数',
  `maxsuccessions` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '最大连续登录天数',
  `prevtime` bigint(16) NULL DEFAULT NULL COMMENT '上次登录时间',
  `logintime` bigint(16) NULL DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
  `joinip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '加入IP',
  `jointime` bigint(16) NULL DEFAULT NULL COMMENT '加入时间',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'Token',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '状态',
  `verification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '验证',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE,
  INDEX `email`(`email`) USING BTREE,
  INDEX `mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user
-- ----------------------------
INSERT INTO `fa_user` VALUES (1, 1, 'admin', 'admin', 'ec7818a113f764560169328efce5a5b5', 'ca067d', 'admin@163.com', '13000000000', 'http://ccc.choujiang.com/assets/img/avatar.png', 0, 0, '2017-04-08', '', 0.00, 0, 1, 1, 1491635035, 1491635035, '127.0.0.1', 0, '127.0.0.1', 1491635035, 0, 1491635035, '', 'normal', '');

-- ----------------------------
-- Table structure for fa_user_group
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_group`;
CREATE TABLE `fa_user_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '组名',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限节点',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '添加时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_group
-- ----------------------------
INSERT INTO `fa_user_group` VALUES (1, '默认组', '1,2,3,4,5,6,7,8,9,10,11,12', 1491635035, 1491635035, 'normal');

-- ----------------------------
-- Table structure for fa_user_money_log
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_money_log`;
CREATE TABLE `fa_user_money_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '变更余额',
  `before` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '变更前余额',
  `after` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '变更后余额',
  `memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员余额变动表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_money_log
-- ----------------------------

-- ----------------------------
-- Table structure for fa_user_rule
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_rule`;
CREATE TABLE `fa_user_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) NULL DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标题',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) NULL DEFAULT NULL COMMENT '是否菜单',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NULL DEFAULT 0 COMMENT '权重',
  `status` enum('normal','hidden') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_rule
-- ----------------------------
INSERT INTO `fa_user_rule` VALUES (1, 0, 'index', 'Frontend', '', 1, 1491635035, 1491635035, 1, 'normal');
INSERT INTO `fa_user_rule` VALUES (2, 0, 'api', 'API Interface', '', 1, 1491635035, 1491635035, 2, 'normal');
INSERT INTO `fa_user_rule` VALUES (3, 1, 'user', 'User Module', '', 1, 1491635035, 1491635035, 12, 'normal');
INSERT INTO `fa_user_rule` VALUES (4, 2, 'user', 'User Module', '', 1, 1491635035, 1491635035, 11, 'normal');
INSERT INTO `fa_user_rule` VALUES (5, 3, 'index/user/login', 'Login', '', 0, 1491635035, 1491635035, 5, 'normal');
INSERT INTO `fa_user_rule` VALUES (6, 3, 'index/user/register', 'Register', '', 0, 1491635035, 1491635035, 7, 'normal');
INSERT INTO `fa_user_rule` VALUES (7, 3, 'index/user/index', 'User Center', '', 0, 1491635035, 1491635035, 9, 'normal');
INSERT INTO `fa_user_rule` VALUES (8, 3, 'index/user/profile', 'Profile', '', 0, 1491635035, 1491635035, 4, 'normal');
INSERT INTO `fa_user_rule` VALUES (9, 4, 'api/user/login', 'Login', '', 0, 1491635035, 1491635035, 6, 'normal');
INSERT INTO `fa_user_rule` VALUES (10, 4, 'api/user/register', 'Register', '', 0, 1491635035, 1491635035, 8, 'normal');
INSERT INTO `fa_user_rule` VALUES (11, 4, 'api/user/index', 'User Center', '', 0, 1491635035, 1491635035, 10, 'normal');
INSERT INTO `fa_user_rule` VALUES (12, 4, 'api/user/profile', 'Profile', '', 0, 1491635035, 1491635035, 3, 'normal');

-- ----------------------------
-- Table structure for fa_user_score_log
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_score_log`;
CREATE TABLE `fa_user_score_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `score` int(10) NOT NULL DEFAULT 0 COMMENT '变更积分',
  `before` int(10) NOT NULL DEFAULT 0 COMMENT '变更前积分',
  `after` int(10) NOT NULL DEFAULT 0 COMMENT '变更后积分',
  `memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员积分变动表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_score_log
-- ----------------------------

-- ----------------------------
-- Table structure for fa_user_token
-- ----------------------------
DROP TABLE IF EXISTS `fa_user_token`;
CREATE TABLE `fa_user_token`  (
  `token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Token',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `expiretime` bigint(16) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员Token表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_user_token
-- ----------------------------

-- ----------------------------
-- Table structure for fa_version
-- ----------------------------
DROP TABLE IF EXISTS `fa_version`;
CREATE TABLE `fa_version`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `oldversion` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '旧版本号',
  `newversion` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '包大小',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '升级内容',
  `downloadurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '强制更新',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '版本表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_version
-- ----------------------------

-- ----------------------------
-- Table structure for fa_win
-- ----------------------------
DROP TABLE IF EXISTS `fa_win`;
CREATE TABLE `fa_win`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动名称',
  `images` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '奖品图片列表',
  `json` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配置:key=名称,value=值',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '朋友圈二维码',
  `views` int(10) NULL DEFAULT NULL COMMENT '点击量',
  `weigh` int(10) NULL DEFAULT NULL COMMENT '排序',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `deletetime` bigint(16) NULL DEFAULT NULL COMMENT '删除时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '抽奖活动表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fa_win
-- ----------------------------
INSERT INTO `fa_win` VALUES (1, '测试', '/uploads/20240627/a8d8528514fcd398a59d87b2538f2b51.jpg,/uploads/20240627/9102827f18d955ac21fda0142570cc62.jpg,/uploads/20240627/2621ef646bad734f5a2cd54fa4f7e418.jpg,/uploads/20240627/9260809b822aa8c080b78922a4aee743.jpg,/uploads/20240627/9a870e83dd89bfcfe2461bcf088e2fd9.jpg,/uploads/20240627/f10aeea5cb1c56040d04d00f441b7bc9.png', '[{\"name\":\"iphone Vision Pro\"},{\"name\":\"电动摩托\"},{\"name\":\"iPhone手表\"},{\"name\":\"NVIDIA RTX4090\"},{\"name\":\"iphone15\"},{\"name\":\"谢谢参与\"}]', '/uploads/20240627/ef6965e014419e8145fd8f37d43e176f.png', 0, 1, 1719388605, NULL, 1719471099);

SET FOREIGN_KEY_CHECKS = 1;
